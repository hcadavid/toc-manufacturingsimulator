
import edu.eci.manufacture.Simulator;

import edu.eci.simulationdescriptor.xlsconverter.InvalidSpreadsheedDataException;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out
                    .println("usage: Manufacture <run id> <configuration file>");
        } else {
            try {
                Simulator simulator = new Simulator(Integer.parseInt(args[0]),
                        args[1]);

                simulator.startup();
            } catch (InvalidSpreadsheedDataException | FileNotFoundException ex) {
                System.out.println(args[1] + ": configuration error " + ex);
            }
        }
    }
}
