package edu.eci.time.arithmetic.calcfactory;

import edu.eci.time.arithmetic.calcfactory.LaborTimeCalculator;
import edu.eci.time.arithmetic.calcfactory.DailyNonLaborTimeInterval;
import edu.eci.time.arithmetic.TimeUnit;

public abstract class LaborTimeCalculatorFactory {

    public static LaborTimeCalculatorFactory getInstance(TimeUnit tu) {
        if (tu == TimeUnit.HOUR) {
            return new edu.eci.time.arithmetic.calcfactory.hourbased.TimeCalculatorFactory();
        } else if (tu == TimeUnit.MINUTE) {
            return new edu.eci.time.arithmetic.calcfactory.minutebased.TimeCalculatorFactory();
        } else {
            throw new RuntimeException("Time unit not supported yet.");
        }
    }

    public abstract LaborTimeCalculator getCalculator();

    public abstract DailyNonLaborTimeInterval createDailyTimeInterval(int st, int et);

    public abstract WeeklyNonLaborTimeInterval createWeeklyTimeInterval(int st, int et);

    public abstract WeekLaborCalendar createWeekLaborCalendar();
}
