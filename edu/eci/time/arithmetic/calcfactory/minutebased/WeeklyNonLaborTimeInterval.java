package edu.eci.time.arithmetic.calcfactory.minutebased;

import edu.eci.time.arithmetic.TimeUnit;

public class WeeklyNonLaborTimeInterval extends edu.eci.time.arithmetic.calcfactory.WeeklyNonLaborTimeInterval {

    public WeeklyNonLaborTimeInterval(int startTimeUnit,
            int endTimeUnit) {
        super(startTimeUnit, endTimeUnit);
    }

    @Override
    public int getUnitsInAWeek() {
        return 10080;
    }

    @Override
    public TimeUnit getTimeUnit() {
        return TimeUnit.MINUTE;
    }
}
