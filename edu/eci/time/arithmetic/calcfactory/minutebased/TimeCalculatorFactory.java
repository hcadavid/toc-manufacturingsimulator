package edu.eci.time.arithmetic.calcfactory.minutebased;

import edu.eci.time.arithmetic.calcfactory.minutebased.LaborTimeCalculator;
import edu.eci.time.arithmetic.calcfactory.minutebased.DailyNonLaborTimeInterval;
import edu.eci.time.arithmetic.calcfactory.LaborTimeCalculatorFactory;

public class TimeCalculatorFactory extends LaborTimeCalculatorFactory {

    private static final LaborTimeCalculator calc = new LaborTimeCalculator();

    @Override
    public LaborTimeCalculator getCalculator() {
        return calc;
    }

    @Override
    public DailyNonLaborTimeInterval createDailyTimeInterval(int st, int et) {
        return new DailyNonLaborTimeInterval(st, et);
    }

    @Override
    public WeeklyNonLaborTimeInterval createWeeklyTimeInterval(int st, int et) {
        return new WeeklyNonLaborTimeInterval(st, et);
    }

    @Override
    public WeekLaborCalendar createWeekLaborCalendar() {
        return new WeekLaborCalendar();
    }
}
