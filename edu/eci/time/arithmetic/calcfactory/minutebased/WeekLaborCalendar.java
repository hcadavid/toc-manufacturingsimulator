package edu.eci.time.arithmetic.calcfactory.minutebased;

import edu.eci.time.arithmetic.TimeUnit;

public class WeekLaborCalendar extends edu.eci.time.arithmetic.calcfactory.WeekLaborCalendar {

    @Override
    public int getUnitsInADay() {
        return 1440;
    }

    @Override
    public TimeUnit getTimeUnit() {
        return TimeUnit.MINUTE;
    }
}
