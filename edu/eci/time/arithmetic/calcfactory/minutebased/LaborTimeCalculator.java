package edu.eci.time.arithmetic.calcfactory.minutebased;

import edu.eci.time.arithmetic.TimeUnit;

public class LaborTimeCalculator extends edu.eci.time.arithmetic.calcfactory.LaborTimeCalculator {

    @Override
    public TimeUnit getTimeUnit() {
        return TimeUnit.MINUTE;
    }

    @Override
    public int getUnitsInAWeek() {
        return 10080;
    }
}
