package edu.eci.time.arithmetic.calcfactory;

import edu.eci.time.arithmetic.calcfactory.TimeInterval;
import edu.eci.time.arithmetic.InvalidIntervalException;
import edu.eci.time.arithmetic.TimeUnit;

public abstract class WeeklyNonLaborTimeInterval extends TimeInterval {

    public WeeklyNonLaborTimeInterval(int startTimeUnit, int endTimeUnit) {
        super(startTimeUnit, endTimeUnit);
        if (startTimeUnit >= endTimeUnit) {
            throw new InvalidIntervalException("Invalid daily time interval:{" + startTimeUnit + ".." + endTimeUnit + "}. Time unit:" + getTimeUnit());
        }
        if (startTimeUnit > (getUnitsInAWeek() - 1) || endTimeUnit > getUnitsInAWeek()) {
            throw new InvalidIntervalException("Invalid daily time interval:{" + startTimeUnit + ".." + endTimeUnit + "}. Time unit:" + getTimeUnit());
        }
    }

    public abstract int getUnitsInAWeek();

    public abstract TimeUnit getTimeUnit();
}
