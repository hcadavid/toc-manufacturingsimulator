package edu.eci.time.arithmetic.calcfactory;

import edu.eci.time.arithmetic.InvalidIntervalException;
import edu.eci.time.arithmetic.TimeUnit;

public abstract class DailyNonLaborTimeInterval extends TimeInterval {

    public DailyNonLaborTimeInterval(int startTimeUnit, int endTimeUnit) {
        super(startTimeUnit, endTimeUnit);
        if (startTimeUnit >= endTimeUnit) {
            throw new InvalidIntervalException("Invalid daily time interval:{" + startTimeUnit + ".." + endTimeUnit + "}. Time unit:" + getTimeUnit());
        }
        if (startTimeUnit > (getUnitsInADay() - 1) || getUnitsInADay() > getUnitsInADay()) {
            throw new InvalidIntervalException("Invalid daily time interval:{" + startTimeUnit + ".." + endTimeUnit + "}. Time unit:" + getTimeUnit());
        }

    }

    public abstract int getUnitsInADay();

    public abstract TimeUnit getTimeUnit();
}
