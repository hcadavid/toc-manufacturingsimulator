package edu.eci.time.arithmetic.calcfactory.hourbased;

import edu.eci.time.arithmetic.TimeUnit;

public class WeekLaborCalendar extends edu.eci.time.arithmetic.calcfactory.WeekLaborCalendar {

    @Override
    public int getUnitsInADay() {
        return 24;
    }

    @Override
    public TimeUnit getTimeUnit() {
        return TimeUnit.HOUR;
    }
}
