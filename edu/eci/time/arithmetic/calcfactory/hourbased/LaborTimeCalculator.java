package edu.eci.time.arithmetic.calcfactory.hourbased;

import edu.eci.time.arithmetic.TimeUnit;

public class LaborTimeCalculator extends edu.eci.time.arithmetic.calcfactory.LaborTimeCalculator {

    @Override
    public TimeUnit getTimeUnit() {
        return TimeUnit.HOUR;
    }

    @Override
    public int getUnitsInAWeek() {
        return 168;
    }
}
