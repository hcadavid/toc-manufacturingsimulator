package edu.eci.time.arithmetic.calcfactory.hourbased;

import edu.eci.time.arithmetic.TimeUnit;

public class DailyNonLaborTimeInterval extends edu.eci.time.arithmetic.calcfactory.DailyNonLaborTimeInterval {

    public DailyNonLaborTimeInterval(int startTimeUnit, int endTimeUnit) {
        super(startTimeUnit, endTimeUnit);
        // TODO Auto-generated constructor stub
    }

    @Override
    public int getUnitsInADay() {
        return 24;
    }

    @Override
    public TimeUnit getTimeUnit() {
        return TimeUnit.HOUR;
    }
}
