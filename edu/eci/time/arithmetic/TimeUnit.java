package edu.eci.time.arithmetic;

public enum TimeUnit {

    HOUR, MINUTE, SECOND
}
