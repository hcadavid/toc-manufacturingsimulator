package edu.eci.simulation.agent;

import edu.eci.simulation.Simulator;
import edu.eci.simulation.data.Service;

import edu.eci.mas.Container;

import java.util.LinkedList;
import java.util.List;

public abstract class Agent extends edu.eci.mas.agent.Agent {

    protected double timeStamp;

    private final List<Service> providedServices;

    private Simulator simulator;

    protected Agent() {
        super();
        this.timeStamp = 0.0;
        this.providedServices = new LinkedList<>();
    }

    protected final int getRunId() {
        return this.simulator.getRunId();
    }

    public final Service getService(String serviceId) {
        for (Service service : this.providedServices) {
            if (service.getId().equals(serviceId)) {
                return service;
            }
        }
        return null;
    }

    protected final void addService(Service service) {
        if (!this.providedServices.contains(service)) {
            this.providedServices.add(service);
        } else {
            error("Service " + service.getId() + "already added");
        }
    }

    protected final void timeWarp(double timeStamp) {
        if (timeStamp > this.timeStamp) {
            // move agent's time forward
            this.timeStamp = timeStamp;
        }
    }

    protected final void addTime(double timeStamp) {
        this.timeStamp = this.timeStamp + timeStamp;
    }

    protected final void persistSimulationEvent(String event) {
        System.out.println(event);
    }

    @Override
    protected final void main(Container container) {
        this.simulator = (Simulator) container;
        main(this.simulator);
        for (Service service : this.providedServices) {
            registerService(service);
        }
    }

    protected abstract void main(Simulator simulator);
}
