package edu.eci.simulation;

import edu.eci.mas.Container;

public abstract class Simulator extends Container {

    private final int runId;

    public Simulator(int runId) {
        super();
        this.runId = runId;
    }

    public final int getRunId() {
        return this.runId;
    }
}
