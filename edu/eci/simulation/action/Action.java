package edu.eci.simulation.action;

import edu.eci.simulation.message.Message;

public abstract class Action extends edu.eci.mas.action.Action {

    public Action() {
        super();
    }

    @Override
    public final void execute(String sender, edu.eci.mas.message.Message m) {
        execute(sender, (Message) m);
    }

    protected abstract void execute(String sender, Message m);
}
