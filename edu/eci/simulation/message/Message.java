package edu.eci.simulation.message;

public abstract class Message extends edu.eci.mas.message.Message {

    protected final double timeStamp;

    public Message(double timeStamp) {
        super();
        this.timeStamp = timeStamp;
    }

    public double getTimeStamp() {
        return this.timeStamp;
    }

    @Override
    public String toString() {
        return "Message:" + ":@t(" + this.timeStamp + ")";
    }
}
