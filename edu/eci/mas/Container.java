package edu.eci.mas;

import edu.eci.mas.agent.Agent;
import edu.eci.mas.agent.ContainerAgent;
import edu.eci.mas.data.Service;
import edu.eci.mas.message.Message;

import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;

import jade.core.AID;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;

import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.ShutdownPlatform;

import jade.lang.acl.ACLMessage;

import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

import java.io.IOException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;

public abstract class Container {

    private final String containerAgent;
    private final Runtime runtime;
    private final AgentContainer mainContainer;
    private final Map<String, AgentController> agentControllerMap;
    private final Map<String, Agent> agentMap;
    private final List<String> agentList;
    private final Object startupLock;
    private final Map<String, List<String>> serviceMap;

    private String mainAgent;

    protected Container() {
        super();
        // Get a hold on JADE runtime
        this.runtime = Runtime.instance();
        // Exit the JVM when there are no more containers around
        this.runtime.setCloseVM(true);
        // Create a default profile
        Profile profile = new ProfileImpl("127.0.0.1", 3505, "MainContainer");
        this.mainContainer = this.runtime.createMainContainer(profile);
        this.agentControllerMap = new HashMap<>();
        this.agentMap = new HashMap<>();
        this.agentList = new LinkedList<>();
        this.startupLock = new Object();
        this.containerAgent = "ContainerAgent";
        createAgent(this.containerAgent, ContainerAgent.class,
                new Object[]{this});
        this.serviceMap = new HashMap<>();
    }

    public final void sendMessage(Agent agent, String destination,
            Message message) {
        try {
            AID id = new AID();
            ACLMessage aclm = new ACLMessage(ACLMessage.INFORM);

            id.setLocalName(destination);
            aclm.setSender(agent.getAID());
            aclm.addReceiver(id);
            aclm.setContentObject(message);
            agent.send(aclm);
        } catch (IOException ex) {
            error(getClass().getCanonicalName()
                    + ".sendMessage: Unable to send message " + message
                    + " from agent " + agent.getLocalName() + " to agent "
                    + destination, ex);
        }
    }

    public final void registerAgent(String agentId, Agent agent) {
        if (!this.agentMap.containsKey(agentId)) {
            this.agentMap.put(agentId, agent);
        } else {
            error(getClass().getCanonicalName() + ".registerAgent: Agent "
                    + agentId + "already registered");
        }
    }

    public final void agentInitialization(String agent) {
        synchronized (this.startupLock) {
            if (this.agentList.contains(agent)) {
                this.agentList.remove(agent);
            }
            if (this.agentList.isEmpty()) {
                this.startupLock.notify();
            }
        }
    }

    public final void print(String m) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.err.println(sdf.format(new Date()) + "," + m + ",");
    }

    public final void error(String errorMsg) {
        throw new RuntimeException(errorMsg);
    }

    public final void error(String errorMsg, Exception ex) {
        throw new RuntimeException(errorMsg, ex);
    }

    public final void startup() {
        main();
        CheckMainAgent();
        startAgents();
    }

    public final void shutdown() {
        stop();
        stopAgents();
        stopPlatform(this.agentMap.get(this.containerAgent));
        this.agentControllerMap.clear();
        this.agentMap.clear();
    }

    public final void registerService(Service service, Agent agent) {
        String serviceId = service.getId();
        String agentId = agent.getLocalName();
        List<String> serviceList;

        if (this.serviceMap.containsKey(serviceId)) {
            serviceList = this.serviceMap.get(serviceId);
            if (!serviceList.contains(agentId)) {
                serviceList.add(agentId);
            }
        } else {
            serviceList = new LinkedList<>();
            serviceList.add(agentId);
            this.serviceMap.put(serviceId, serviceList);
        }
    }

    public final List<String> searchService(String serviceName) {
        List<String> agentList2;

        if (this.serviceMap.containsKey(serviceName)) {
            agentList2 = this.serviceMap.get(serviceName);
        } else {
            agentList2 = new LinkedList<>();
        }
        return agentList2;
    }

    protected final void createAgent(String id,
            Class<? extends jade.core.Agent> agent, Object[] arguments) {
        try {
            if (this.agentControllerMap.get(id) == null) {
                Object[] args = new Object[arguments.length + 1];

                args[0] = this;
                System.arraycopy(arguments, 0, args, 1, arguments.length);

                AgentController agentController = this.mainContainer
                        .createNewAgent(id, agent.getCanonicalName(), args);

                this.agentControllerMap.put(id, agentController);
            } else {
                error(getClass().getCanonicalName() + ".createAgent: Agent "
                        + id + " already created: " + agent.getCanonicalName());
            }
        } catch (StaleProxyException ex) {
            error(getClass().getCanonicalName() + ".createAgent: Agent " + id
                    + " creation failed: " + agent.getCanonicalName(), ex);
        }
    }

    protected final void setMainAgent(String mainAgent) {
        this.mainAgent = mainAgent;
    }

    protected abstract void main();

    protected abstract void stop();

    private void CheckMainAgent() {
        if (this.mainAgent == null) {
            error(getClass().getCanonicalName()
                    + ".checkMainAgent: mainAgent not registered");
        } else if (this.agentControllerMap.get(this.mainAgent) == null) {
            error(getClass().getCanonicalName() + ".checkMainAgent: mainAgent "
                    + this.mainAgent + " not created");
        }
    }

    private void startAgents() {
        synchronized (this.startupLock) {
            try {
                for (String agent : this.agentControllerMap.keySet()) {
                    this.agentList.add(agent);
                }
                this.agentList.remove(this.mainAgent);
                for (String agent : this.agentList) {
                    this.agentControllerMap.get(agent).start();
                }
                this.startupLock.wait();
                this.agentList.add(this.mainAgent);
                this.agentControllerMap.get(this.mainAgent).start();
                this.startupLock.wait();
            } catch (StaleProxyException | InterruptedException ex) {
                error(getClass().getCanonicalName()
                        + ".startAgents: start/wait failed", ex);
            }
        }
    }

    private void stopAgents() {
        for (String agent : this.agentMap.keySet()) {
            this.agentMap.get(agent).shutdown();
        }
    }

    private void stopPlatform(Agent containerAgent) {
        try {
            Codec codec = new SLCodec();
            Ontology jmo = JADEManagementOntology.getInstance();
            ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);

            ContentManager contentManager = containerAgent.getContentManager();
            contentManager.registerLanguage(codec);
            contentManager.registerOntology(jmo);
            msg.addReceiver(containerAgent.getAMS());
            msg.setLanguage(codec.getName());
            msg.setOntology(jmo.getName());
            contentManager.fillContent(msg, new Action(containerAgent.getAID(),
                    new ShutdownPlatform()));
            containerAgent.send(msg);
        } catch (CodecException | OntologyException ex) {
            error(getClass().getCanonicalName() + ".stopPlatform: error", ex);
        }
    }
}
