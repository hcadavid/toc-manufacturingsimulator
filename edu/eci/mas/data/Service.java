package edu.eci.mas.data;

public abstract class Service extends Data {

    private final String id;

    public Service(String id) {
        super();
        this.id = id;
    }

    public String getId() {
        return this.id;
    }
}
