package edu.eci.mas.agent;

import edu.eci.mas.Container;

public final class ContainerAgent extends Agent {

    public ContainerAgent() {
        super();
    }

    @Override
    protected void addActions() {
    }

    @Override
    protected void main(Container Container) {
    }

    @Override
    protected void stop() {
    }
}
