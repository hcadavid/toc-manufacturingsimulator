package edu.eci.mas.agent;

import edu.eci.mas.message.Message;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

public final class CyclicBehavior extends jade.core.behaviours.CyclicBehaviour {

    private final Agent agent;

    public CyclicBehavior(Agent agent) {
        super(agent);
        this.agent = (Agent) this.myAgent;
    }

    @Override
    public final void action() {
        try {
            ACLMessage msg = this.agent.receive();

            if (msg != null) {
                if (msg.hasByteSequenceContent()) {
                    Object co = msg.getContentObject();

                    if (co instanceof Message) {
                        this.agent.handleMessage(
                                msg.getSender().getLocalName(), (Message) co);
                    } else {
                        this.agent
                                .error(getClass().getCanonicalName()
                                        + ": Only Message objects are allowed as messages for Agent instances.");
                    }
                } else if (!msg.getContent().contains("shutdown-platform")) {
                    this.agent.error(getClass().getCanonicalName()
                            + ": Unexpected message received for "
                            + this.agent.getLocalName() + ":"
                            + msg.getContent());
                }
            } else {
                block();
            }
        } catch (UnreadableException ex) {
            this.agent
                    .error(getClass().getCanonicalName()
                            + ": An error ocurred while trying to perform a message related action. Agent "
                            + this.agent.getLocalName(), ex);
        }
    }
}
