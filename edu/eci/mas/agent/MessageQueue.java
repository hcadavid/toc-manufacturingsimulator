package edu.eci.mas.agent;

import org.mapdb.DBMaker;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.List;

import java.util.Queue;

public final class MessageQueue implements jade.core.MessageQueue {

    private final Queue<ACLMessage> queue;

    protected MessageQueue(String agent) {

        this.queue = DBMaker.newTempFileDB().asyncWriteEnable()
                .asyncWriteQueueSize(32000)
                .transactionDisable()
                .closeOnJvmShutdown().make()
                .getQueue(agent);
    }

    @Override
    public final void addFirst(ACLMessage message) {
        throw new RuntimeException("Unsupported!");
    }

    @Override
    public final void addLast(ACLMessage message) {
        this.queue.add(message);
    }

    @Override
    public final void copyTo(List list) {
    }

    @Override
    public final int getMaxSize() {
        return 999999999;
    }

    @Override
    public final boolean isEmpty() {
        return queue.isEmpty();
    }

    @Override
    public final ACLMessage receive(MessageTemplate template) {
        ACLMessage message = this.queue.peek();

        if (message != null) {
            this.queue.remove();
        }
        return message;
    }

    @Override
    public final void setMaxSize(int size) {
    }

    @Override
    public final int size() {
        return this.queue.isEmpty() ? 0 : 1;
    }
}
