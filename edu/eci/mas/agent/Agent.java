package edu.eci.mas.agent;

import edu.eci.mas.Container;
import edu.eci.mas.action.Action;
import edu.eci.mas.data.Service;
import edu.eci.mas.message.Message;

import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class Agent extends jade.core.Agent implements IMessageHandler {

    private final Map<String, Action> actionMap;

    private Object[] arguments;
    private Container container;

    protected Agent() {
        super();
        this.actionMap = new LinkedHashMap<>();
    }

    public final void shutdown() {
        stop();
    }

    public final void print(String m) {
        this.container.print(m);
    }

    @Override
    public final Object[] getArguments() {
        return this.arguments;
    }

    @Override
    public final void handleMessage(String sender, Message message) {
        String messageClass = message.getClass().getCanonicalName();
        Action action = this.actionMap.get(messageClass);

        if (action != null) {
            action.execute(sender, message);
        } else {
            error("Received unknow message: " + messageClass + ".");
        }
    }

    @Override
    protected final MessageQueue createMessageQueue() {
        return new MessageQueue(getLocalName());
    }

    protected final void sendMessage(String destination, Message message) {
        this.container.sendMessage(this, destination, message);
    }

    protected final void error(String errorMsg) {
        this.container.error(getClass().getCanonicalName() + ":" + errorMsg);
    }

    protected final void error(String errorMsg, Exception ex) {
        this.container
                .error(getClass().getCanonicalName() + ":" + errorMsg, ex);
    }

    @Override
    protected final void setup() {
        String agent = getLocalName();
        Object[] args = super.getArguments();

        super.setup();
        this.container = (Container) args[0];
        this.container.registerAgent(agent, this);
        this.arguments = new Object[args.length - 1];
        System.arraycopy(args, 1, this.arguments, 0, args.length - 1);
        addBehaviour(new CyclicBehavior(this));
        addActions();
        main(this.container);
        this.container.agentInitialization(agent);
    }

    protected final void registerService(Service service) {
        this.container.registerService(service, this);
    }

    protected final String findAgentByService(String service) {
        List<String> la = findAgentsByService(service);

        if (la.size() != 1) {
            error(getClass().getCanonicalName()
                    + ".findAgentByService() - No agent found for service:"
                    + service);
        }
        return la.get(0);
    }

    protected final void addAction(Class<? extends Message> message,
            Class<? extends Action> action) {
        addAction(message, action, getClass());
    }

    private void addAction(Class<? extends Message> message,
            Class<? extends Action> action, Class<? extends Agent> agent) {
        String messageClass = message.getCanonicalName();
        Action actionInstance = this.actionMap.get(messageClass);

        if (actionInstance == null) {
            createAction(messageClass, action, agent);
        } else if (actionInstance.getClass().getCanonicalName()
                .compareTo(action.getCanonicalName()) != 0) {
            error("Action " + action.getCanonicalName()
                    + " already added to message " + messageClass
                    + " in agent " + agent.getCanonicalName());
        }
    }

    protected abstract void addActions();

    protected abstract void main(Container container);

    protected abstract void stop();

    private List<String> findAgentsByService(String serviceName) {
        return this.container.searchService(serviceName);
    }

    private void createAction(String message, Class<? extends Action> action,
            Class<? extends Agent> agent) {
        try {
            Constructor<? extends Action> ct;
            Action actionInstance;
            Class<?>[] partypes = new Class<?>[1];
            Object[] arglist = new Object[1];

            partypes[0] = agent;
            arglist[0] = this;
            ct = action.getConstructor(partypes);
            actionInstance = (Action) ct.newInstance(arglist);
            this.actionMap.put(message, actionInstance);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            error("CreateAction: Can't add action " + action.getCanonicalName()
                    + " to message " + message + " in agent "
                    + agent.getCanonicalName(), ex);
        }
    }
}
