package edu.eci.mas.agent;

import edu.eci.mas.message.Message;

public interface IMessageHandler {

    public abstract void handleMessage(String sender, Message message);
}
