package edu.eci.mas.action;

import edu.eci.mas.message.Message;

public abstract class Action {

    protected Action() {
        super();
    }

    public abstract void execute(String sender, Message m);
}
