package edu.eci.manufacture;

import edu.eci.manufacture.agent.StoreUnit;
import edu.eci.manufacture.agent.ProductionUnit;
import edu.eci.manufacture.agent.WareHouseUnit;
import edu.eci.manufacture.data.Configuration;

import edu.eci.simulationdescriptor.ProductionOrder;
import edu.eci.simulationdescriptor.xlsconverter.InvalidSpreadsheedDataException;

import java.util.List;
import java.io.FileNotFoundException;

public class Simulator extends edu.eci.simulation.Simulator {

    private final String filename;
    private final Configuration configuration;

    public Simulator(int runId, String filename) throws FileNotFoundException,
            InvalidSpreadsheedDataException {
        super(runId);
        this.filename = filename;
        this.configuration = new Configuration(this.filename);
    }

    public final List<ProductionOrder> getProductionOrders() {
        return this.configuration.getProductionOrders();
    }

    @Override
    protected final void main() {
        print("Simulator Started");
        createAgent("store", StoreUnit.class, new Object[]{});
        for (String puname : this.configuration.getProductionUnitNames()) {
            createAgent(puname, ProductionUnit.class,
                    this.configuration.getProductionUnitArguments(puname));
        }
        createAgent("warehouse", WareHouseUnit.class,
                this.configuration.getWareHouseUnitArguments());
        setMainAgent("store");
        print("Agents Created");
    }

    @Override
    protected void stop() {
        print("Simulator Ended");
    }
}
