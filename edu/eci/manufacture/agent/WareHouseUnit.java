package edu.eci.manufacture.agent;

import edu.eci.manufacture.Simulator;
import edu.eci.manufacture.action.WareHouseSimulate;
import edu.eci.manufacture.action.WareHouseRequest;
import edu.eci.manufacture.data.PendingRawMaterialRequest;
import edu.eci.manufacture.data.RawMaterial;
import edu.eci.manufacture.data.Recipe;
import edu.eci.manufacture.message.Request;
import edu.eci.manufacture.message.RequestAcknowledge;
import edu.eci.manufacture.message.RequestPool;
import edu.eci.manufacture.message.Simulate;
import edu.eci.manufacture.message.Response;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.LinkedHashMap;

public class WareHouseUnit extends Agent {

    private final Map<Long, List<PendingRawMaterialRequest>> requestMap;
    private final Map<String, RawMaterial> rawMaterialMap;
    private final Map<String, Double> bufferMap;

    public WareHouseUnit() {
        super();
        this.requestMap = new LinkedHashMap<>();
        this.rawMaterialMap = new LinkedHashMap<>();
        this.bufferMap = new LinkedHashMap<>();
    }

    public final void addRequest(String agent, Request request) {
        int requestId = RequestPool.getInstance().addRequest(request);
        long context = request.getContext();
        PendingRawMaterialRequest pendReq = new PendingRawMaterialRequest(
                agent, requestId);

        checkContext(context);
        this.requestMap.get(context).add(pendReq);
        sendMessage(agent, new RequestAcknowledge(context));
    }

    public final void dispatch(long context) {
        Runtime runtime = Runtime.getRuntime();

        List<PendingRawMaterialRequest> requestList = this.requestMap
                .get(context);
        Object[] array = requestList.toArray();

        requestList.clear();
        Arrays.sort(array);
        for (Object request : array) {
            dispatch((PendingRawMaterialRequest) request);
        }
        System.gc();
    }

    @Override
    protected final void addActions() {
        addAction(Request.class, WareHouseRequest.class);
        addAction(Simulate.class, WareHouseSimulate.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected final void main(Simulator simulator) {
        Object[] args = getArguments();

        if (args.length == 1) {
            if (args[0] instanceof Map<?, ?>) {
                setup((Map<String, RawMaterial>) args[0]);
            } else {
                error(".setup(). RawMaterial arguments expected for agent "
                        + getLocalName());
            }
        } else {
            error(".setup(). Missing arguments for agent " + getLocalName());
        }
    }

    @Override
    protected final void stop() {
    }

    private void setup(Map<String, RawMaterial> rawMaterials) {
        for (String name : rawMaterials.keySet()) {
            RawMaterial rawMaterial = rawMaterials.get(name);

            this.rawMaterialMap.put(name, rawMaterial);
            addService(new Recipe(name));
            // init warehouse unit bufferMap
            this.bufferMap.put(name, rawMaterial.getInitialQuantity());
            persistWipUpdateEvent(this.timeStamp, name, name,
                    this.bufferMap.get(name));
        }
    }

    private void checkContext(long context) {
        if (!this.requestMap.containsKey(context)) {
            this.requestMap.put(context,
                    new LinkedList<PendingRawMaterialRequest>());
        }
    }

    private void dispatch(PendingRawMaterialRequest pendReq) {
        String reqProduct = pendReq.getProduct();
        double reqTimeStamp = pendReq.getTimeStamp();
        double quantity = this.bufferMap.get(reqProduct);
        double reqQuantity = pendReq.getQuantity();

        timeWarp(reqTimeStamp);
        useBuffer(pendReq);
        if (pendReq.ready()) {
            sendMessage(
                    pendReq.getSender(),
                    new Response(this.timeStamp, reqQuantity, pendReq
                            .getRequest()));
            this.bufferMap.put(reqProduct, quantity - reqQuantity);
            persistWipUpdateEvent(this.timeStamp, reqProduct, reqProduct,
                    this.bufferMap.get(reqProduct));
        } else {
            dispatch(pendReq, reqProduct, quantity, reqQuantity);
        }
    }

    private void useBuffer(PendingRawMaterialRequest pendReq) {
        String reqProduct = pendReq.getProduct();
        double quantity = this.bufferMap.get(reqProduct);

        if (quantity > 0) {
            double count = pendReq.getQuantity() - pendReq.getCount();

            if (quantity > count) {
                pendReq.addProduct(count);
                this.bufferMap.put(reqProduct, quantity - count);
            } else {
                pendReq.addProduct(quantity);
                this.bufferMap.put(reqProduct, 0.0);
            }
            persistWipUpdateEvent(this.timeStamp, reqProduct, reqProduct,
                    quantity);
        }
    }

    private void dispatch(PendingRawMaterialRequest pendReq, String reqProduct,
            double quantity, double reqQuantity) {
        RawMaterial rawMaterial = this.rawMaterialMap.get(reqProduct);
        double rawMaterialQuantity = rawMaterial.getQuantity();

        while (quantity < reqQuantity) {
            quantity = quantity + rawMaterialQuantity;
            addTime(rawMaterial.getTime());
            persistWipUpdateEvent(this.timeStamp, reqProduct, reqProduct,
                    quantity);
        }
        this.bufferMap.put(reqProduct, quantity - reqQuantity);
        persistWipUpdateEvent(this.timeStamp + 0.000000001, reqProduct,
                reqProduct, this.bufferMap.get(reqProduct));
        sendMessage(pendReq.getSender(), new Response(this.timeStamp,
                reqQuantity, pendReq.getRequest()));
    }
}
