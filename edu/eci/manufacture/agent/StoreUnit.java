package edu.eci.manufacture.agent;

import edu.eci.manufacture.Simulator;
import edu.eci.manufacture.action.StoreRequestAcknowledge;
import edu.eci.manufacture.action.StoreResponse;
import edu.eci.manufacture.data.PendingProductionRequest;
import edu.eci.manufacture.message.Request;
import edu.eci.manufacture.message.RequestAcknowledge;
import edu.eci.manufacture.message.RequestPool;
import edu.eci.manufacture.message.Simulate;
import edu.eci.manufacture.message.Response;
import edu.eci.simulationdescriptor.ProductionOrder;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.LinkedHashMap;

public class StoreUnit extends Agent {

    private final Map<Long, PendingProductionRequest> pendingRequestMap;
    private final List<String> agentList;

    private edu.eci.manufacture.Simulator simulator;
    private List<ProductionOrder> productionOrders = null;
    private int requestSize = 0;
    private int requestCount = 0;

    public StoreUnit() {
        super();
        this.pendingRequestMap = new LinkedHashMap<>();
        this.agentList = new LinkedList<>();
    }

    public final void incRequestAcknowledge() {
        this.requestCount++;
        if (this.requestCount == this.requestSize) {
            for (String agent : this.agentList) {
                sendMessage(agent, new Simulate());
            }
        }
    }

    public final void processResponse(String agent, Response response) {
        PendingProductionRequest pendReq = this.pendingRequestMap.get(response
                .getId());
        String pendReqProduct = pendReq.getProduct();
        double responseTimeStamp = response.getTimeStamp();
        double quantity = response.getQuantity();

        pendReq.addProduct(quantity);
        persistWipUpdateEvent(responseTimeStamp, pendReqProduct,
                response.getRecipe(), pendReq.getCount());
        if (ready()) {
            this.simulator.shutdown();
        }
    }

    @Override
    protected final void main(Simulator simulator) {
        Object[] args = getArguments();

        if (args.length == 0) {
            this.simulator = simulator;
            this.productionOrders = this.simulator.getProductionOrders();
            this.requestSize = this.productionOrders.size();
            sendRequests();
        } else {
            error(".main: Too many arguments for agent " + getLocalName());
        }
    }

    @Override
    protected final void stop() {
    }

    @Override
    protected final void addActions() {
        addAction(RequestAcknowledge.class, StoreRequestAcknowledge.class);
        addAction(Response.class, StoreResponse.class);
    }

    private void sendRequests() {
        List<String> productRequestList = new LinkedList<>();
        String sender = getLocalName();

        for (ProductionOrder po : this.productionOrders) {
            String product = po.getProductId();
            String agent = findAgentByService(product);
            double time = po.getTime();
            double quantity = po.getAmount();
            Request request = new Request(product, time, quantity);
            PendingProductionRequest pendReq = new PendingProductionRequest(
                    sender, RequestPool.getInstance().addRequest(request));
            long id = pendReq.getId();

            if (!productRequestList.contains(product)) {
                persistWipUpdateEvent(this.timeStamp, product, "",
                        pendReq.getCount());
                productRequestList.add(product);
            }
            this.pendingRequestMap.put(id, pendReq);
            if (!this.agentList.contains(agent)) {
                this.agentList.add(agent);
            }
            sendMessage(agent, request);
        }
        productRequestList.clear();
    }

    private boolean ready() {
        for (long id : this.pendingRequestMap.keySet()) {
            if (!this.pendingRequestMap.get(id).ready()) {
                return false;
            }
        }
        return true;
    }
}
