package edu.eci.manufacture.agent;

import edu.eci.manufacture.Simulator;

public abstract class Agent extends edu.eci.simulation.agent.Agent {

    private Simulator simulator;

    /**
     *
     * @param timeStamp simulation timestamp
     * @param product product name
     * @param recipe product recipe name
     * @param newWip new wip value
     */
    protected final void persistPuUpdateEvent(double timeStamp, String product,
            String recipe, double newWip) {
        String agent = getLocalName();
        StringBuilder punitid = new StringBuilder(getLocalName());

        if (!agent.contains("@")) {
            if (!recipe.equals("")) {
                punitid.append("@");
                punitid.append(recipe);
            }
        }
        persistSimulationEvent(getPUTSV(getRunId(), timeStamp,
                punitid.toString(), product, newWip));
    }

    /**
     *
     * @param timeStamp simulation timestamp
     * @param product product name
     * @param recipe product recipe name
     * @param newWip new wip value
     */
    protected final void persistWipUpdateEvent(double timeStamp,
            String product, String recipe, double newWip) {
        String agent = getLocalName();
        StringBuilder punitid = new StringBuilder(getLocalName());

        if (!agent.contains("@")) {
            if (!recipe.equals("")) {
                punitid.append("@");
                punitid.append(recipe);
            }
        }
        persistSimulationEvent(getWipUpdateTSV(getRunId(), timeStamp,
                punitid.toString(), product, newWip));
    }

    @Override
    protected final void main(edu.eci.simulation.Simulator simulator) {
        this.simulator = (Simulator) simulator;
        main(this.simulator);

    }

    protected abstract void main(Simulator simulator);

    private String getPUTSV(int runid, double simtime, String punitId,
            String prodRedIf, double newWip) {
        StringBuilder sb = new StringBuilder();
        sb.append(runid);
        sb.append("\t");
        sb.append("\"PU\"");
        sb.append("\t\"");

        /*
         * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         * sb.append(sdf.format(new Date())); sb.append("\"\t");
         */
        sb.append(simtime);
        sb.append("\t\"");
        sb.append(punitId);
        sb.append("\"\t\"");
        sb.append(prodRedIf);
        sb.append("\"\t");
        sb.append(newWip);
        sb.append("\t");
        sb.append("\\N");
        sb.append("\t");
        sb.append("\\N");
        return sb.toString();
    }

    private String getWipUpdateTSV(int runid, double simtime, String punitId,
            String prodRedIf, double newWip) {
        StringBuilder sb = new StringBuilder();

        sb.append(runid);
        sb.append("\t");
        sb.append("\"WU\"");
        sb.append("\t\"");

        /*
         * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         * sb.append(sdf.format(new Date())); sb.append("\"\t");
         */
        sb.append(simtime);
        sb.append("\t\"");
        sb.append(punitId);
        sb.append("\"\t\"");
        sb.append(prodRedIf);
        sb.append("\"\t");
        sb.append(newWip);
        sb.append("\t");
        sb.append("\\N");
        sb.append("\t");
        sb.append("\\N");
        return sb.toString();
    }

}
