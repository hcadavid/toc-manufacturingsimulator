package edu.eci.manufacture.agent;

import edu.eci.manufacture.Simulator;
import edu.eci.manufacture.action.ProductionRequest;
import edu.eci.manufacture.action.ProductionRequestAcknowledge;
import edu.eci.manufacture.action.ProductionResponse;
import edu.eci.manufacture.action.ProductionSimulate;
import edu.eci.manufacture.data.PendingProductionRequest;
import edu.eci.manufacture.data.ComponentProductionResponse;
import edu.eci.manufacture.data.Recipe;
import edu.eci.manufacture.data.Component;
import edu.eci.manufacture.message.Request;
import edu.eci.manufacture.message.RequestAcknowledge;
import edu.eci.manufacture.message.RequestPool;
import edu.eci.manufacture.message.Simulate;
import edu.eci.manufacture.message.Response;

import gnu.trove.map.hash.TLongObjectHashMap;
import gnu.trove.iterator.TLongObjectIterator;
import gnu.trove.map.hash.TLongDoubleHashMap;
import gnu.trove.map.hash.TLongLongHashMap;

import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Objects;

public class ProductionUnit extends Agent {

    static private long componentContextIdSequence = 1;

    private final TLongObjectHashMap<List<PendingProductionRequest>> requestMap;
    private final TLongObjectHashMap<PendingProductionRequest> pendingRequestMap;
    private final TLongObjectHashMap<TLongObjectHashMap<List<Response>>> responseMap;
    private final Map<String, Double> bufferMap;
    private final Map<String, Recipe> recipeMap;
    private final Map<String, Double> batchSizeMap;
    private final TLongDoubleHashMap requestQuantityMap;
    private final TLongDoubleHashMap responseQuantityMap;
    private final TLongLongHashMap requestAcknowledgeCountMap;
    private final TLongLongHashMap requestAcknowledgeSizeMap;
    private final TLongObjectHashMap<List<String>> agentRequestMap;
    private final TLongLongHashMap contextMap;

    private int firstPending;

    public ProductionUnit() {
        super();
        this.requestMap = new TLongObjectHashMap<>();
        this.pendingRequestMap = new TLongObjectHashMap<>();
        this.responseMap = new TLongObjectHashMap<>();
        this.bufferMap = new LinkedHashMap<>();
        this.recipeMap = new LinkedHashMap<>();
        this.batchSizeMap = new LinkedHashMap<>();
        this.requestQuantityMap = new TLongDoubleHashMap();
        this.requestAcknowledgeCountMap = new TLongLongHashMap();
        this.requestAcknowledgeSizeMap = new TLongLongHashMap();
        this.responseQuantityMap = new TLongDoubleHashMap();
        this.agentRequestMap = new TLongObjectHashMap<>();
        this.contextMap = new TLongLongHashMap();
    }

    public final void addRequest(String agent, Request request) {
        int requestId = RequestPool.getInstance().addRequest(request);
        long context = request.getContext();
        PendingProductionRequest pendReq = new PendingProductionRequest(agent,
                this.batchSizeMap.get(request.getProduct()), requestId);

        checkContext(context);
        this.requestMap.get(context).add(pendReq);
        sendMessage(agent, new RequestAcknowledge(context));
    }

    public final void incRequestAcknowledge(long componentContext) {
        long context = this.contextMap.get(componentContext);

        this.requestAcknowledgeCountMap.put(context,
                this.requestAcknowledgeCountMap.get(context) + 1);
        if (Objects.equals(this.requestAcknowledgeCountMap.get(context), this.requestAcknowledgeSizeMap
                .get(context))) {
            for (String agent : this.agentRequestMap.get(context)) {
                sendMessage(agent, new Simulate(componentContext));
            }
        }
    }

    public final void manufacture(long context) {
        List<PendingProductionRequest> pendingRequestList = new LinkedList<>();

        processRequests(context, pendingRequestList);
        sendComponentRequests(context, pendingRequestList);
        pendingRequestList.clear();
    }

    public final void addResponse(String agent, Response response) {
        long context = this.contextMap.get(response.getContext());
        String product = response.getProduct();

        this.responseQuantityMap.put(context,
                this.responseQuantityMap.get(context) + response.getQuantity());
        this.responseMap.get(context).get(response.getOriginalId())
                .add(response);
        if (!(this.responseQuantityMap.get(context) < this.requestQuantityMap
                .get(context))) {
            Runtime runtime = Runtime.getRuntime();

            /*print(getLocalName() + ",processResponses,Start," + context
                    + "," + product + ","
                    + this.responseQuantityMap.get(context) + ","
                    + this.requestQuantityMap.get(context));*/
            processResponses(context);
            System.gc();
            /*print(getLocalName() + ",processResponses,End," + context + ","
                    + product + ",");*/
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Production Unit for");
        for (String name : this.recipeMap.keySet()) {
            Recipe recipe = this.recipeMap.get(name);
            String product = recipe.getId();

            sb.append(" ").append(product);
            sb.append(":(");
            for (Object object : recipe.getComponents()) {
                Component component = (Component) object;
                String cname = component.getName();

                sb.append(" ").append(cname);
            }
            sb.append(" )");
        }
        return sb.toString();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected final void main(Simulator simulator) {
        Object[] args = getArguments();

        if (args.length == 3) {
            if (args[0] instanceof Map<?, ?>) {
                if (args[1] instanceof Map<?, ?>) {
                    if (args[2] instanceof Map<?, ?>) {
                        setup((Map<String, Recipe>) args[0],
                                (Map<String, Double>) args[1],
                                (Map<String, Double>) args[2]);
                    } else {
                        error(".main() - Missing argument for agent "
                                + getLocalName() + ": batch sizes.");
                    }
                } else {
                    error(".main() - Missing argument for agent "
                            + getLocalName() + ": initial quantities.");
                }
            } else {
                error(".main() - Missing argument for agent " + getLocalName()
                        + ": provided services (recipes).");
            }
        } else {
            error(".main() - Invalid argument(s) for agent " + getLocalName());
        }
    }

    @Override
    protected final void stop() {
    }

    @Override
    protected final void addActions() {
        addAction(Request.class, ProductionRequest.class);
        addAction(RequestAcknowledge.class, ProductionRequestAcknowledge.class);
        addAction(Simulate.class, ProductionSimulate.class);
        addAction(Response.class, ProductionResponse.class);
    }

    private void setup(Map<String, Recipe> recipes,
            Map<String, Double> initialQuantities,
            Map<String, Double> batchSizes) {
        for (String product : recipes.keySet()) {
            Map<String, LinkedList<ComponentProductionResponse>> available = new LinkedHashMap<>();
            Recipe recipe = recipes.get(product);

            this.recipeMap.put(product, recipe);
            this.bufferMap.put(product, initialQuantities.get(product));
            this.batchSizeMap.put(product, batchSizes.get(product));
            addService(recipe);
            // init production unit's buffers
            persistWipUpdateEvent(this.timeStamp, product, product,
                    this.bufferMap.get(product));
            // init recipe components buffers
            for (Component component : recipe.getComponents()) {
                String cname = component.getName();

                this.bufferMap.put(cname, component.getInitialQuantity());
                available.put(cname,
                        new LinkedList<ComponentProductionResponse>());
                persistWipUpdateEvent(this.timeStamp, cname, product,
                        this.bufferMap.get(cname));
            }
        }
    }

    private void checkContext(long context) {
        if (!this.requestMap.containsKey(context)) {
            this.requestMap.put(context,
                    new LinkedList<PendingProductionRequest>());
            this.requestQuantityMap.put(context, 0.0);
            this.responseMap.put(context,
                    new TLongObjectHashMap<List<Response>>());
            this.responseQuantityMap.put(context, 0.0);
            this.requestAcknowledgeCountMap.put(context, 0L);
            this.requestAcknowledgeSizeMap.put(context, 0L);
        }
    }

    private void useBuffer(PendingProductionRequest pendReq) {
        String product = pendReq.getProduct();
        Recipe recipe = this.recipeMap.get(product);
        String component = recipe.getId();
        double quantity = this.bufferMap.get(component);

        if (quantity > 0.0) {
            double newCount = pendReq.getQuantity() - pendReq.getCount();

            if (quantity > newCount) {
                pendReq.addProduct(newCount);
                this.bufferMap.put(component, quantity - newCount);
            } else {
                pendReq.addProduct(quantity);
                this.bufferMap.put(component, 0.0);
            }
            sendMessage(pendReq.getSender(),
                    new Response(pendReq.getTimeStamp(), pendReq.getCount(),
                            pendReq.getRequest()));
            persistPuUpdateEvent(this.timeStamp + 0.000000001, product,
                    product, this.bufferMap.get(component));
        }
    }

    private void processRequests(long context,
            List<PendingProductionRequest> pendingRequestList) {
        List<PendingProductionRequest> requestList = this.requestMap
                .get(context);
        Object[] requestArray = requestList.toArray();

        requestList.clear();
        Arrays.sort(requestArray);
        for (Object request : requestArray) {
            PendingProductionRequest pendReq = (PendingProductionRequest) request;
            Recipe recipe = this.recipeMap.get(pendReq.getProduct());
            useBuffer(pendReq);
            if (!pendReq.ready()) {
                pendingRequestList.add(pendReq);
                this.requestAcknowledgeSizeMap.put(
                        context,
                        this.requestAcknowledgeSizeMap.get(context)
                        + recipe.getComponents().length);
            }
        }
    }

    private void sendComponentRequests(long context,
            List<PendingProductionRequest> pendingRequestList) {
        long componentContext = componentContextIdSequence++;
        List<String> agentList = new LinkedList<>();

        this.contextMap.put(componentContext, context);
        for (PendingProductionRequest pendReq : pendingRequestList) {
            long requestId = pendReq.getId();
            String product = pendReq.getProduct();
            Recipe recipe = this.recipeMap.get(product);
            TLongObjectHashMap<List<Response>> responseMap2 = this.responseMap
                    .get(context);

            if (responseMap2 == null) {
                responseMap2 = new TLongObjectHashMap<>();
                this.responseMap.put(context, responseMap2);
            }
            this.pendingRequestMap.put(requestId, pendReq);
            if (!responseMap2.containsKey(requestId)) {
                responseMap2.put(requestId, new LinkedList<Response>());
            }
            for (Component component : recipe.getComponents()) {
                String cname = component.getName();
                String agent = findAgentByService(cname);
                Request request = new Request(requestId, cname, product,
                        pendReq.getTimeStamp(),
                        (pendReq.getQuantity() - pendReq.getCount())
                        * component.getQuantity(), componentContext);

                if (!agentList.contains(agent)) {
                    agentList.add(agent);
                }
                // recipe request is sent with the original timestamp by
                // default, the request takes 0 unit of time
                this.requestQuantityMap.put(
                        context,
                        this.requestQuantityMap.get(context)
                        + request.getQuantity());
                sendMessage(agent, request);
            }
        }
        this.agentRequestMap.put(context, agentList);
    }

    private void processResponses(long context) {
        TLongObjectHashMap<List<Response>> responseMap2 = this.responseMap.get(context);
        TLongObjectIterator<List<Response>> iterator = responseMap2.iterator();

        for (int i = responseMap2.size(); i > 0; i--) {
            iterator.advance();

            List<Response> responseList = responseMap2.get(iterator.key());
            Object[] array = responseList.toArray();
            int size = array.length;

            responseList.clear();
            Arrays.sort(array);
            this.firstPending = -1;
            for (int pos = 0; pos < size; pos++) {
                processResponse(pos, (Response) array[pos]);
            }
            if (this.firstPending != -1) {
                processResponse((Response) array[this.firstPending]);
            }
        }
        responseMap2.clear();
        this.responseMap.remove(context);
    }

    private void processResponse(int pos, Response response) {
        double responseTimeStamp = response.getTimeStamp();
        String responseProduct = response.getProduct();
        double responseQuantity = response.getQuantity();
        Recipe recipe = this.recipeMap.get(response.getRecipe());
        String product = recipe.getId();

        addComponent(product, responseProduct, responseTimeStamp,
                responseQuantity);
        if (responseTimeStamp < this.timeStamp) {
            if (this.firstPending == -1) {
                this.firstPending = pos;
            }
        } else {
            this.firstPending = -1;
            processResponse(response, responseTimeStamp, recipe,
                    product);
        }
    }

    private void addComponent(String product, String component,
            double timeStamp, double quantity) {
        this.bufferMap.put(component, this.bufferMap.get(component) + quantity);
        persistWipUpdateEvent(timeStamp, component, product,
                this.bufferMap.get(component));
    }

    private void processResponse(Response response,
            double responseTimeStamp, Recipe recipe, String product) {
        PendingProductionRequest pendReq = this.pendingRequestMap.get(response
                .getOriginalId());

        timeWarp(responseTimeStamp);
        while (!pendReq.ready() && productReady(recipe)) {
            addProduct(pendReq, recipe, product, recipe.getTime());
            if (pendReq.batchReady()) {
                sendResponse(pendReq, product);
            }
        }
        if (pendReq.ready() && !pendReq.batchReady()) {
            sendResponse(pendReq, product, pendReq.getBatchLength());
        }
    }

    private void processResponse(Response response) {
        double responseTimeStamp = response.getTimeStamp();
        Recipe recipe = this.recipeMap.get(response.getRecipe());
        String product = recipe.getId();

        processResponse(response, responseTimeStamp, recipe, product);
    }

    private boolean productReady(Recipe product) {
        for (Component component : product.getComponents()) {
            if (this.bufferMap.get(component.getName()).compareTo(
                    component.getQuantity()) < 0) {
                return false;
            }
        }
        return true;
    }

    private void addProduct(PendingProductionRequest pendReq, Recipe recipe,
            String product, double time) {
        addTime(time);
        // set status as request with another product
        // manufactured
        pendReq.addProduct();
        reduceWorkInProgress(recipe);
        this.bufferMap.put(product, this.bufferMap.get(product) + 1);
        persistPuUpdateEvent(this.timeStamp, product, product,
                this.bufferMap.get(product));
    }

    private void sendResponse(PendingProductionRequest pendReq, String product) {
        double batchSize = pendReq.getBatchSize();

        sendMessage(pendReq.getSender(), new Response(this.timeStamp,
                batchSize, pendReq.getRequest()));
        this.bufferMap.put(product, this.bufferMap.get(product) - batchSize);
        double quantity = this.bufferMap.get(product);
        if (quantity < 0.000000001) {
            persistPuUpdateEvent(this.timeStamp + 0.000000001, product,
                    product, quantity);
        } else {
            persistPuUpdateEvent(this.timeStamp, product, product, quantity);
        }
    }

    private void sendResponse(PendingProductionRequest pendReq, String product,
            double quantity) {
        sendMessage(pendReq.getSender(), new Response(this.timeStamp, quantity,
                pendReq.getRequest()));
        this.bufferMap.put(product, 0.0);
        persistPuUpdateEvent(this.timeStamp + 0.000000001, product, product,
                this.bufferMap.get(product));
    }

    private void reduceWorkInProgress(Recipe recipe) {
        String product = recipe.getId();

        for (Component component : recipe.getComponents()) {
            String cname = component.getName();

            this.bufferMap.put(cname,
                    this.bufferMap.get(cname) - component.getQuantity());
            persistWipUpdateEvent(this.timeStamp, cname, product,
                    this.bufferMap.get(cname));
        }
    }
}
