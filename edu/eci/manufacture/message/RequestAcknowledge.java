package edu.eci.manufacture.message;

public class RequestAcknowledge extends Message {

    private final long context;

    public RequestAcknowledge(long context) {
        super(0L);
        this.context = context;
    }

    public final long getContext() {
        return this.context;
    }

    @Override
    public String toString() {
        return "[RequestAcknowledge:" + this.context + "]";
    }
}
