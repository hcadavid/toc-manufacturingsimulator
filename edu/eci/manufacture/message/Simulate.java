package edu.eci.manufacture.message;

public class Simulate extends Message {

    private final long context;

    public Simulate() {
        super(0L);
        this.context = 0L;
    }

    public Simulate(long context) {
        super(0L);
        this.context = context;
    }

    public final long getContext() {
        return this.context;
    }

    @Override
    public String toString() {
        return "[Simulate:" + this.context + "]";
    }
}
