package edu.eci.manufacture.message;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class RequestPool {

    private static final RequestPool instance = new RequestPool();

    private final HashMap<Integer, Request> requests;
    private final HashMap<Request, Integer> requestIds;
    private final Object poolLock;

    private int poolIdCount;

    protected RequestPool() {
        this.requests = new LinkedHashMap<>();
        this.requestIds = new LinkedHashMap<>();
        this.poolLock = new Object();
        this.poolIdCount = 0;
    }

    public int addRequest(Request request) {
        synchronized (this.poolLock) {
            this.requests.put(this.poolIdCount, request);
            this.requestIds.put(request, this.poolIdCount);
            return this.poolIdCount++;
        }
    }

    public Request getRequest(int requestId) {
        return this.requests.get(requestId);
    }

    public static RequestPool getInstance() {
        return instance;
    }
}
