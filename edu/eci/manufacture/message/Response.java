package edu.eci.manufacture.message;

import java.util.Comparator;

public class Response extends Message implements Comparable<Response>,
        Comparator<Response> {

    private final double quantity;
    private final int request;

    public Response(double timeStamp, double quantity, int request) {
        super(timeStamp);
        this.quantity = quantity;
        this.request = request;
    }

    public double getQuantity() {
        return this.quantity;
    }

    public long getContext() {
        return RequestPool.getInstance().getRequest(this.request).getContext();
    }

    public long getId() {
        return RequestPool.getInstance().getRequest(this.request).getId();
    }

    public long getOriginalId() {
        return RequestPool.getInstance().getRequest(this.request).getOriginalId();
    }

    public String getProduct() {
        return RequestPool.getInstance().getRequest(this.request).getProduct();
    }

    public String getRecipe() {
        return RequestPool.getInstance().getRequest(this.request).getRecipe();
    }

    @Override
    public int compareTo(Response o) {
        return Double.compare(this.timeStamp, o.timeStamp);
    }

    @Override
    public int compare(Response o1, Response o2) {
        return Double.compare(o1.timeStamp, o2.timeStamp);
    }

    @Override
    public String toString() {
        return "[Response:" + this.timeStamp + " " + this.quantity + " "
                + RequestPool.getInstance().getRequest(this.request) + "]";
    }

}
