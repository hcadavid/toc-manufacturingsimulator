package edu.eci.manufacture.message;

public abstract class Message extends edu.eci.simulation.message.Message {

    public Message(double timeStamp) {
        super(timeStamp);
    }
}
