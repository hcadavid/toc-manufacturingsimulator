package edu.eci.manufacture.message;

public class Request extends Message {

    private static long requestIdSequence = 1L;

    private final long id;
    private final long originalId;
    private final String product;
    private final String recipe;
    private final double quantity;
    private final long context;

    public Request(String product, double timeStamp, double quantity) {
        super(timeStamp);
        this.id = requestIdSequence++;
        this.originalId = 0L;
        this.product = product;
        this.recipe = "";
        this.quantity = Check(quantity);
        this.context = 0L;
    }

    public Request(long originalId, String product, String recipe, double timeStamp,
            double quantity, long context) {
        super(timeStamp);
        this.id = requestIdSequence++;
        this.originalId = originalId;
        this.product = product;
        this.recipe = recipe;
        this.quantity = Check(quantity);
        this.context = context;
    }

    public long getOriginalId() {
        return this.originalId;
    }

    public long getId() {
        return this.id;
    }

    public String getProduct() {
        return this.product;
    }

    public String getRecipe() {
        return this.recipe;
    }

    public double getQuantity() {
        return this.quantity;
    }

    public long getContext() {
        return this.context;
    }

    @Override
    public String toString() {
        return "[Request:" + this.id + " " + this.originalId + " " + this.product + " " + this.recipe
                + " " + this.timeStamp + " " + this.quantity + " "
                + this.context + "]";
    }
    
    private double Check(double quantity) {
    	double q = 0.0;
    	
    	while (q < quantity) {
    		q = q + 1;
    	}
    	return q;
    }
}
