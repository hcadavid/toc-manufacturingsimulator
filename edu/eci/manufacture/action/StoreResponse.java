package edu.eci.manufacture.action;

import edu.eci.manufacture.agent.StoreUnit;
import edu.eci.manufacture.message.Response;
import edu.eci.manufacture.message.Message;

public class StoreResponse extends Action {

    private final StoreUnit agent;

    public StoreResponse(StoreUnit agent) {
        super();
        this.agent = agent;
    }

    @Override
    public final void execute(String sender, Message message) {
        Response response = (Response) message;

        this.agent.processResponse(sender, response);
    }
}
