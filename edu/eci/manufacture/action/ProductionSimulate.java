package edu.eci.manufacture.action;

import edu.eci.manufacture.agent.ProductionUnit;
import edu.eci.manufacture.message.Simulate;
import edu.eci.manufacture.message.Message;

public class ProductionSimulate extends Action {

    private final ProductionUnit agent;

    public ProductionSimulate(ProductionUnit agent) {
        super();
        this.agent = agent;
    }

    @Override
    public void execute(String sender, Message message) {
        Simulate simulate = (Simulate) message;

        this.agent.manufacture(simulate.getContext());
    }
}
