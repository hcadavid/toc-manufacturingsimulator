package edu.eci.manufacture.action;

import edu.eci.manufacture.agent.ProductionUnit;
import edu.eci.manufacture.message.RequestAcknowledge;
import edu.eci.manufacture.message.Message;

public class ProductionRequestAcknowledge extends Action {

    private final ProductionUnit agent;

    public ProductionRequestAcknowledge(ProductionUnit agent) {
        super();
        this.agent = agent;
    }

    @Override
    protected void execute(String sender, Message message) {
        RequestAcknowledge requestAcknowledge = (RequestAcknowledge) message;

        this.agent.incRequestAcknowledge(requestAcknowledge.getContext());
    }
}
