package edu.eci.manufacture.action;

import edu.eci.manufacture.agent.ProductionUnit;
import edu.eci.manufacture.message.Request;
import edu.eci.manufacture.message.Message;

public class ProductionRequest extends Action {

    private final ProductionUnit agent;

    public ProductionRequest(ProductionUnit agent) {
        super();
        this.agent = agent;
    }

    @Override
    public void execute(String sender, Message message) {
        Request request = (Request) message;

        this.agent.addRequest(sender, request);
    }
}
