package edu.eci.manufacture.action;

import edu.eci.manufacture.message.Message;

public abstract class Action extends edu.eci.simulation.action.Action {

    @Override
    protected void execute(String sender, edu.eci.simulation.message.Message message) {
        execute(sender, (Message) message);
    }

    protected abstract void execute(String sender, Message message);

}
