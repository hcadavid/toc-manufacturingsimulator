package edu.eci.manufacture.action;

import edu.eci.manufacture.agent.StoreUnit;
import edu.eci.manufacture.message.Message;

public class StoreRequestAcknowledge extends Action {

    private final StoreUnit agent;

    public StoreRequestAcknowledge(StoreUnit agent) {
        super();
        this.agent = agent;
    }

    @Override
    protected void execute(String sender, Message message) {
        this.agent.incRequestAcknowledge();
    }
}
