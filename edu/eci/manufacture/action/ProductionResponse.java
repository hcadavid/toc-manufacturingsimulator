package edu.eci.manufacture.action;

import edu.eci.manufacture.message.Response;
import edu.eci.manufacture.agent.ProductionUnit;
import edu.eci.manufacture.message.Message;

public class ProductionResponse extends Action {

    private final ProductionUnit agent;

    public ProductionResponse(ProductionUnit agent) {
        super();
        this.agent = agent;
    }

    @Override
    public void execute(String sender, Message message) {
        Response response = (Response) message;

        this.agent.addResponse(sender, response);
    }
}
