package edu.eci.manufacture.action;

import edu.eci.manufacture.agent.WareHouseUnit;
import edu.eci.manufacture.message.Simulate;
import edu.eci.manufacture.message.Message;

public class WareHouseSimulate extends Action {

    private final WareHouseUnit agent;

    public WareHouseSimulate(WareHouseUnit agent) {
        super();
        this.agent = agent;
    }

    @Override
    protected void execute(String sender, Message message) {
        Simulate simulate = (Simulate) message;

        this.agent.dispatch(simulate.getContext());
    }
}
