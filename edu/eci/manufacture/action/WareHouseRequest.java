package edu.eci.manufacture.action;

import edu.eci.manufacture.agent.WareHouseUnit;
import edu.eci.manufacture.message.Request;
import edu.eci.manufacture.message.Message;

public class WareHouseRequest extends Action {

    private final WareHouseUnit agent;

    public WareHouseRequest(WareHouseUnit agent) {
        super();
        this.agent = agent;
    }

    @Override
    public void execute(String sender, Message message) {
        Request request = (Request) message;

        this.agent.addRequest(sender, request);
    }
}
