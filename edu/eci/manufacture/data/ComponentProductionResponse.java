package edu.eci.manufacture.data;

public class ComponentProductionResponse {

    private final double timeStamp;

    private double quantity;

    public ComponentProductionResponse(double timeStamp, double quantity) {
        this.timeStamp = timeStamp;
        this.quantity = quantity;
    }

    public final double getTimeStamp() {
        return this.timeStamp;
    }

    public final double getQuantity() {
        return this.quantity;
    }

    public void reduceQuantity(double quantity) {
        this.quantity -= quantity;
    }

    @Override
    public String toString() {
        return "[Request:" + this.timeStamp + " " + this.quantity + "]";
    }
}
