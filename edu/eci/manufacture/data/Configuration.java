package edu.eci.manufacture.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.eci.simulationdescriptor.ProductionOrder;
import edu.eci.simulationdescriptor.RawMaterialSource;
import edu.eci.simulationdescriptor.SimulationDescriptor;
import edu.eci.simulationdescriptor.xlsconverter.InvalidSpreadsheedDataException;
import edu.eci.simulationdescriptor.xlsconverter.SpreadSheetModelDataReader;

public class Configuration {

    private final String filename;
    private final SimulationDescriptor sd;
    private final List<ProductionOrder> productionOrders;
    private final List<String> rawMaterials;
    private final List<String> units;
    private final List<String> products;
    private final List<String> productionUnits;
    private final Map<String, Map<String, Recipe>> recipes;
    private final Map<String, Map<String, Double>> initialQuantities;
    ;
	private final Map<String, Map<String, Double>> batchSizes;

    public Configuration(String filename) throws FileNotFoundException,
            InvalidSpreadsheedDataException {
        this.filename = filename;
        this.productionOrders = SpreadSheetModelDataReader
                .getProductionOrdersFromSpreadsheet(new FileInputStream(
                                this.filename));
        this.sd = SpreadSheetModelDataReader
                .getDescriptorFromSpreadsheet(new FileInputStream(this.filename));
        this.rawMaterials = new LinkedList<>();
        this.units = new LinkedList<>();
        this.products = new LinkedList<>();
        this.productionUnits = new LinkedList<>();
        this.recipes = new HashMap<>();
        this.initialQuantities = new HashMap<>();
        this.batchSizes = new HashMap<>();
        getMaterials();
        getProductionUnits();
    }

    public final List<ProductionOrder> getProductionOrders() {
        return this.productionOrders;
    }

    public final List<String> getProductionUnitNames() {
        return this.productionUnits;
    }

    public final Object[] getProductionUnitArguments(String puname) {
        return new Object[]{this.recipes.get(puname),
            this.initialQuantities.get(puname),
            this.batchSizes.get(puname)};
    }

    public final Object[] getWareHouseUnitArguments() {
        Set<RawMaterialSource> rawMaterialSources = this.sd
                .getRawMaterialSources();
        Map<String, RawMaterial> rawMaterials2 = new HashMap<>();

        for (RawMaterialSource rawMaterialSource : rawMaterialSources) {
            String name = rawMaterialSource.getComponentName();

            if (this.rawMaterials.contains(name)) {
                rawMaterials2.put(name, new RawMaterial(name,
                        rawMaterialSource.getMinimumOrderSize(),
                        rawMaterialSource.getDeliveryTime(), 0.0));
                this.rawMaterials.remove(name);
            }
        }
        for (String name : this.rawMaterials) {
            rawMaterials2.put(name, new RawMaterial(name, 1.0, 1.0, 0.0));
        }
        return new Object[]{rawMaterials2};
    }

    private void getMaterials() {
        List<String> productionOrders2 = new LinkedList<>();
        int added;

        for (ProductionOrder po : this.productionOrders) {
            String product = po.getProductId();

            if (!productionOrders2.contains(product)) {
                productionOrders2.add(product);
            }
        }
        do {
            added = 0;
            for (edu.eci.simulationdescriptor.ProductionUnit pu : this.sd
                    .getProductionUnits()) {
                String puname = pu.getName();

                for (edu.eci.simulationdescriptor.Recipe r : pu.getRecipes()) {
                    String name = r.getOutcomeName();

                    if (productionOrders2.contains(name)) {
                        productionOrders2.remove(name);
                        if (this.rawMaterials.contains(name)) {
                            this.rawMaterials.remove(name);
                        }
                        if (!this.products.contains(name)) {
                            this.products.add(name);
                            if (!this.units.contains(puname)) {
                                this.units.add(puname);
                            }
                        }
                        for (edu.eci.simulationdescriptor.Component c : r
                                .getComponents()) {
                            String cname = c.getName();

                            if (!this.products.contains(cname)
                                    && !this.rawMaterials.contains(cname)) {
                                this.rawMaterials.add(cname);
                            }
                            if (!productionOrders2.contains(cname)) {
                                productionOrders2.add(cname);
                                added++;
                            }
                        }
                    }
                }
            }
        } while (added > 0);
    }

    private void getProductionUnits() {
        for (edu.eci.simulationdescriptor.ProductionUnit pu : this.sd
                .getProductionUnits()) {
            String puname = pu.getName();

            if (this.units.contains(puname)) {
                this.units.remove(puname);
                getRecipes(pu, puname);
            }
        }
    }

    private void getRecipes(edu.eci.simulationdescriptor.ProductionUnit pu,
            String puname) {
        Map<String, Double> initialWip = pu.getInitialWip();

        for (edu.eci.simulationdescriptor.Recipe recipe : pu.getRecipes()) {
            String name = recipe.getOutcomeName();

            if (this.products.contains(name)) {
                Component[] components = getComponents(recipe, initialWip);

                if (!this.productionUnits.contains(puname)) {
                    this.productionUnits.add(puname);
                }
                addRecipe(puname, name,
                        new Recipe(name, components, recipe.getRequiredTime()));
                addInitialQuantity(
                        puname,
                        name, initialWip.containsKey(name) ? initialWip
                        .get(name) : 0.0);
                addBatchSize(puname, name, recipe.getBatchSize());
            }
        }
    }

    private void addRecipe(String puname, String name, Recipe recipe) {
        Map<String, Recipe> map = this.recipes.get(puname);

        if (map == null) {
            map = new HashMap<>();
            this.recipes.put(puname, map);
        }
        if (!map.containsKey(name)) {
            map.put(name, recipe);
        }
    }

    private void addInitialQuantity(String puname, String name, Double quantity) {
        Map<String, Double> map = this.initialQuantities.get(puname);

        if (map == null) {
            map = new HashMap<>();
            this.initialQuantities.put(puname, map);
        }
        if (!map.containsKey(name)) {
            map.put(name, quantity);
        }
    }

    private void addBatchSize(String puname, String product,
            double quantity) {
        Map<String, Double> map = this.batchSizes.get(puname);

        if (map == null) {
            map = new HashMap<>();
            this.batchSizes.put(puname, map);
        }
        if (!map.containsKey(product)) {
            map.put(product, quantity);
        }
    }

    private Component[] getComponents(
            edu.eci.simulationdescriptor.Recipe recipe,
            Map<String, Double> initialWip) {
        List<Component> componentList = new LinkedList<>();

        for (edu.eci.simulationdescriptor.Component c : recipe.getComponents()) {
            String cname = c.getName();

            componentList.add(new Component(cname, c.getAmount(), initialWip
                    .containsKey(cname) ? initialWip.get(cname) : 0.0));
        }
        Component[] components = new Component[componentList.size()];

        int i = 0;
        for (Component c : componentList) {
            components[i++] = c;
        }
        return components;
    }
}
