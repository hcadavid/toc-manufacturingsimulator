package edu.eci.manufacture.data;

import edu.eci.manufacture.message.RequestPool;
import edu.eci.simulation.data.Data;

import java.util.Comparator;

public class PendingProductionRequest extends Data implements
        Comparable<PendingProductionRequest>,
        Comparator<PendingProductionRequest> {

    private final String sender;
    private final double batchSize;
    private final int request;

    private double count;
    private double batchCount;
    private double batchLength;

    public PendingProductionRequest(String sender, int request) {
        super();
        this.sender = sender;
        this.batchSize = RequestPool.getInstance().getRequest(request)
                .getQuantity();
        this.request = request;
        this.count = 0.0;
        this.batchCount = 0.0;
        this.batchLength = 0.0;
    }

    public PendingProductionRequest(String sender, double batchSize, int request) {
        super();
        this.sender = sender;
        this.batchSize = batchSize;
        this.request = request;
        this.count = 0.0;
        this.batchCount = 0.0;
        this.batchLength = 0.0;
    }

    public final String getSender() {
        return this.sender;
    }

    public final double getBatchSize() {
        return this.batchSize;
    }

    public final int getRequest() {
        return this.request;
    }

    public final double getCount() {
        return this.count;
    }

    public final double getBatchLength() {
        return this.batchLength;
    }

    public final long getId() {
        return RequestPool.getInstance().getRequest(this.request).getId();
    }

    public final long getOriginalId() {
        return RequestPool.getInstance().getRequest(this.request)
                .getOriginalId();
    }

    public final double getTimeStamp() {
        return RequestPool.getInstance().getRequest(this.request)
                .getTimeStamp();
    }

    public final String getProduct() {
        return RequestPool.getInstance().getRequest(this.request).getProduct();
    }

    public final String getRecipe() {
        return RequestPool.getInstance().getRequest(this.request).getRecipe();
    }

    public final double getQuantity() {
        return RequestPool.getInstance().getRequest(this.request).getQuantity();
    }

    public final long getContext() {
        return RequestPool.getInstance().getRequest(this.request).getContext();
    }

    public final void addProduct() {
        this.count = this.count + 1.0;
        this.batchLength = this.batchLength + 1.0;
        if (!(this.batchLength < this.batchSize)) {
            this.batchLength = this.batchLength - this.batchSize;
            this.batchCount = this.batchCount + this.batchSize;
        }
    }

    public final void addProduct(double quantity) {
        this.count = this.count + quantity;
        this.batchLength = this.batchLength + quantity;
        while (!(this.batchLength < this.batchSize)) {
            this.batchLength = this.batchLength - this.batchSize;
            this.batchCount = this.batchCount + this.batchSize;
        }
    }

    public final boolean ready() {
        return !(this.count < RequestPool.getInstance()
                .getRequest(this.request).getQuantity());
    }

    public final boolean batchReady() {
        return !(this.batchCount < this.count);
    }

    @Override
    public int compare(PendingProductionRequest o1, PendingProductionRequest o2) {
        return Double
                .compare(RequestPool.getInstance().getRequest(o1.request)
                        .getTimeStamp(),
                        RequestPool.getInstance().getRequest(o2.request)
                        .getTimeStamp());
    }

    @Override
    public int compareTo(PendingProductionRequest o) {
        return Double.compare(RequestPool.getInstance()
                .getRequest(this.request).getTimeStamp(), RequestPool
                .getInstance().getRequest(o.request).getTimeStamp());
    }

    @Override
    public final String toString() {
        return "[PendingProductionRequest:" + this.batchSize + " "
                + RequestPool.getInstance().getRequest(this.request) + " "
                + this.count + " " + this.batchCount + "]";
    }
}
