package edu.eci.manufacture.data;

import edu.eci.simulation.data.Data;

public class Component extends Data {

    private final String name;
    private final double quantity;
    private final double initialQuantity;

    public Component(String name) {
        super();
        this.name = name;
        this.quantity = 1.0;
        this.initialQuantity = 0.0;
    }

    public Component(String name, double quantity) {
        this.name = name;
        this.quantity = quantity;
        this.initialQuantity = 0.0;
    }

    public Component(String name, double quantity, double initialQuantity) {
        this.name = name;
        this.quantity = quantity;
        this.initialQuantity = initialQuantity;
    }

    public final String getName() {
        return this.name;
    }

    public final double getQuantity() {
        return this.quantity;
    }

    public final double getInitialQuantity() {
        return this.initialQuantity;
    }

    @Override
    public String toString() {
        return "[Component:" + this.name + " " + this.quantity + "]";
    }
}
