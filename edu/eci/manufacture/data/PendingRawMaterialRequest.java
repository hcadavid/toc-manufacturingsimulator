package edu.eci.manufacture.data;

import edu.eci.manufacture.message.RequestPool;
import edu.eci.simulation.data.Data;

import java.util.Comparator;

public class PendingRawMaterialRequest extends Data implements
        Comparable<PendingRawMaterialRequest>,
        Comparator<PendingRawMaterialRequest> {

    private final String sender;
    private final int request;
    private double count;

    public PendingRawMaterialRequest(String sender, int request) {
        super();
        this.sender = sender;
        this.request = request;
        this.count = 0.0;
    }

    public String getSender() {
        return this.sender;
    }

    public int getRequest() {
        return this.request;
    }

    public final double getCount() {
        return this.count;
    }

    public long getId() {
        return RequestPool.getInstance().getRequest(this.request).getId();
    }

    public double getTimeStamp() {
        return RequestPool.getInstance().getRequest(this.request)
                .getTimeStamp();
    }

    public String getProduct() {
        return RequestPool.getInstance().getRequest(this.request).getProduct();
    }

    public String getRecipe() {
        return RequestPool.getInstance().getRequest(this.request).getRecipe();
    }

    public double getQuantity() {
        return RequestPool.getInstance().getRequest(this.request).getQuantity();
    }

    public long getContext() {
        return RequestPool.getInstance().getRequest(this.request).getContext();
    }

    public final void addProduct(double quantity) {
        this.count = this.count + quantity;
    }

    public final boolean ready() {
        return !(this.count < RequestPool.getInstance()
                .getRequest(this.request).getQuantity());
    }

    @Override
    public int compare(PendingRawMaterialRequest o1,
            PendingRawMaterialRequest o2) {
        return Double
                .compare(RequestPool.getInstance().getRequest(o1.request)
                        .getTimeStamp(),
                        RequestPool.getInstance().getRequest(o2.request)
                        .getTimeStamp());
    }

    @Override
    public int compareTo(PendingRawMaterialRequest o) {
        return Double.compare(RequestPool.getInstance()
                .getRequest(this.request).getTimeStamp(), RequestPool
                .getInstance().getRequest(o.request).getTimeStamp());
    }

    @Override
    public String toString() {
        return "[PendingRawMaterialRequest " + this.sender + " "
                + RequestPool.getInstance().getRequest(this.request) + " "
                + this.count + "]";
    }
}
