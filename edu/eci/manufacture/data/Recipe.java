package edu.eci.manufacture.data;

import edu.eci.simulation.data.Service;

public class Recipe extends Service {

    private final Component[] components;
    private final double time;

    public Recipe(String name, Component[] components, double time) {
        super(name);
        this.components = components;
        this.time = time;
    }

    public Recipe(String name, double time) {
        super(name);
        this.components = new Component[]{};
        this.time = time;
    }

    public Recipe(String name) {
        super(name);
        this.components = new Component[]{};
        this.time = 0.0;
    }

    public Component[] getComponents() {
        return this.components;
    }

    public double getTime() {
        return this.time;
    }

    @Override
    public String toString() {
        String rc;

        rc = "[Recipe:" + this.time + "<";
        for (Component c : this.components) {
            rc = rc + " " + c;
        }
        return rc + ">]";
    }
}
