package edu.eci.manufacture.data;

import edu.eci.simulation.data.Data;

public class RawMaterial extends Data {

    private final String name;
    private final double quantity;
    private final double time;
    private final double initialQuantity;

    public RawMaterial(String name, double quantity, double time,
            double initialQuantity) {
        this.name = name;
        this.quantity = quantity;
        this.time = time;
        this.initialQuantity = initialQuantity;
    }

    public final String getName() {
        return this.name;
    }

    public final double getQuantity() {
        return this.quantity;
    }

    public final double getTime() {
        return this.time;
    }

    public final double getInitialQuantity() {
        return this.initialQuantity;
    }

    @Override
    public final String toString() {
        return "[RawMaterial:" + this.name + " " + this.quantity + " "
                + this.time + "," + this.initialQuantity + "]";
    }
}
